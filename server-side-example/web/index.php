<?php
require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

$client = new GuzzleHttp\Client();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/templates',
));

$app->get('/static/{file}', function ($file) use ($app, $client) {
    $filepath = "static/" . $file;

    if (file_exists($filepath)) {
        $content = file_get_contents($filepath);
        $response = new Symfony\Component\HttpFoundation\Response($content, 200);
        $response->headers->set('Content-Type', 'text/css');
        $response->headers->set('Content-Disposition', 'attachment');
        return $response;
    }
});

$app->get('/', function () use ($app, $client) {
    $hourly = weatherNextHours($client)['hourly']['data'];

    $datosHoras = [];
    for ($i = 0; $i < count($hourly); ++$i) {
        $date = new DateTime('now', new DateTimeZone('UTC'));
        $date->setTimestamp($hourly[$i]['time']);
        $date->setTimezone(new DateTimeZone('America/Santiago'));

        $datosHoras[] = [
            'hora' => $date->format('H:i'),
            'pronostico' => $hourly[$i]['summary'],
            'temperatura' => round($hourly[$i]['temperature'],0),
            'humedad' => $hourly[$i]['humidity'] * 100
        ];
    }

    $currently = weatherNow($client)['currently'];

    $date = new DateTime('now', new DateTimeZone('UTC'));
    $date->setTimestamp($currently['time']);
    $date->setTimezone(new DateTimeZone('America/Santiago'));

    return $app['twig']->render('now.html', array(
        'baseURL' => $actual_link = "http://$_SERVER[HTTP_HOST]/web/index.php",
        'hora' => $date->format('H:i:s'),
        'pronostico' => $currently['summary'],
        'temperatura' => round($currently['temperature'],0),
        'humedad' => $currently['humidity'] * 100,
        'datos' => $datosHoras
    ));
});

$app->get('/week', function () use ($app, $client) {
    $daily = weatherNextDays($client)['daily']['data'];

    $datosSemana = [];
    for ($i = 0; $i < count($daily); ++$i) {
        $date = new DateTime('now', new DateTimeZone('UTC'));
        $date->setTimestamp($daily[$i]['time']);
        $date->setTimezone(new DateTimeZone('America/Santiago'));

        $datosSemana[] = [
            'hora' => $date->format('d-m'),
            'pronostico' => $daily[$i]['summary'],
            'temperaturaMax' => round($daily[$i]['temperatureHigh'],0),
            'temperaturaMin' => round($daily[$i]['temperatureMin'],0),
            'humedad' => $daily[$i]['humidity'] * 100
        ];
    }

    return $app['twig']->render('week.html', array(
        'baseURL' => $actual_link = "http://$_SERVER[HTTP_HOST]/web/index.php",
        'datos' => $datosSemana
    ));
});

// @codeCoverageIgnoreStart
//if (PHP_SAPI != 'cli') {
$app->run();
//}
// @codeCoverageIgnoreEnd

return $app;

// llamada a API REST Dark Sky

function weatherNow($client)
{
    $res = $client->request('GET', 'https://api.darksky.net/forecast/67f3b5e4d29180cf5c62e28d8cee1c7b/-33.439274,-70.647640?exclude=hourly,daily,minutely&lang=es&units=si');
    return \GuzzleHttp\json_decode($res->getBody(), true);
}

function weatherNextHours($client)
{
    $res = $client->request('GET', 'https://api.darksky.net/forecast/67f3b5e4d29180cf5c62e28d8cee1c7b/-33.439274,-70.647640?exclude=currently,daily,minutely&lang=es&units=si');
    return \GuzzleHttp\json_decode($res->getBody(), true);
}

function weatherNextDays($client)
{
    $res = $client->request('GET', 'https://api.darksky.net/forecast/67f3b5e4d29180cf5c62e28d8cee1c7b/-33.439274,-70.647640?exclude=currently,hourly,minutely&lang=es&units=si');
    return \GuzzleHttp\json_decode($res->getBody(), true);
}