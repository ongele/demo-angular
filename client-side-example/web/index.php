<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

$client = new GuzzleHttp\Client();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Headers', 'Authorization');
});

$app->get('/static/{file}', function ($file) use ($app, $client) {
    $filepath = "static/" . $file;

    if (file_exists($filepath)) {
        $content = file_get_contents($filepath);
        $response = new Symfony\Component\HttpFoundation\Response($content, 200);
        $response->headers->set('Content-Type', 'text/css');
        $response->headers->set('Content-Disposition', 'attachment');
        return $response;
    }
});

$app->get('/', function () use ($app, $client) {
    return $app['twig']->render('now.html', array(
        'baseURL' => $actual_link = "http://$_SERVER[HTTP_HOST]/web/index.php",));
});

$app->get('/api/now', function () use ($app, $client) {
    return $app->json(weatherNow($client));
});

$app->get('/api/nextHours', function () use ($app, $client) {
    return $app->json(weatherNextHours($client));
});

$app->get('/api/nextDays', function () use ($app, $client) {
    return $app->json(weatherNextDays($client));
});

// @codeCoverageIgnoreStart
//if (PHP_SAPI != 'cli') {
$app->run();
//}
// @codeCoverageIgnoreEnd

return $app;

// llamada a API REST Dark Sky

function weatherNow($client)
{
    $res = $client->request('GET', 'https://api.darksky.net/forecast/67f3b5e4d29180cf5c62e28d8cee1c7b/-33.439274,-70.647640?exclude=hourly,daily,minutely&lang=es&units=si');
    return \GuzzleHttp\json_decode($res->getBody(), true);
}

function weatherNextHours($client)
{
    $res = $client->request('GET', 'https://api.darksky.net/forecast/67f3b5e4d29180cf5c62e28d8cee1c7b/-33.439274,-70.647640?exclude=currently,daily,minutely&lang=es&units=si');
    return \GuzzleHttp\json_decode($res->getBody(), true);
}

function weatherNextDays($client)
{
    $res = $client->request('GET', 'https://api.darksky.net/forecast/67f3b5e4d29180cf5c62e28d8cee1c7b/-33.439274,-70.647640?exclude=currently,hourly,minutely&lang=es&units=si');
    return \GuzzleHttp\json_decode($res->getBody());
}