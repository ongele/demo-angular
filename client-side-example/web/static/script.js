var nowURL = "http://localhost:8000/web/index.php/api/now";
var todayURL = "http://localhost:8000/web/index.php/api/nextHours";
var weekURL = "http://localhost:8000/web/index.php/api/nextDays";

var nowDone, nextHoursDone, nextDaysDone = false;

function weatherNow() {
    if (nowDone) {
        return;
    }

    nowDone = true;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var datos = JSON.parse(this.response).currently;

            document.getElementById("tableNow").innerHTML = "<tbody>" +
                "<tr>" +
                "<td><b>Hora</b></td>" +
                "<td>" + moment(datos.time * 1000).format('HH:mm:ss') + "</td>" +
                "</tr><tr>" +
                "<td><b>Pronóstico</b></td>" +
                "<td>" + datos.summary + "</td>" +
                "</tr><tr>" +
                "<td><b>Temperatura</b></td>" +
                "<td>" + Math.round(datos.temperature) + "ºC</td>" +
                "</tr><tr>" +
                "<td><b>Humedad</b></td>" +
                "<td>" + Math.round(datos.humidity * 100) + "%</td>" +
                "</tr></tbody>";
        }
    };
    xhttp.open("GET", nowURL, true);
    xhttp.send();
}

function weatherNextHours() {
    if (nextHoursDone) {
        return;
    }

    nextHoursDone = true;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var datos = JSON.parse(this.response).hourly;

            var s = "<tbody>" +
                "<tr><th>Hora</th><th>Pronóstico</th><th>Temperatura</th><th>Humedad</th></tr>";

            for (var i = 0; i < datos.data.length; ++i) {
                var dato = datos.data[i];

                s += "<tr><td>" + moment(dato.time*1000).format('HH:mm') + "</td>" +
                    "<td>" + dato.summary + "</td>" +
                    "<td>" + Math.round(dato.temperature) + "ºC</td>" +
                    "<td>" + Math.round(dato.humidity * 100) + "%</td>" +
                    "</tr>"
            }
            s += "</tbody>";

            document.getElementById("tableToday").innerHTML = s;

        }
    };
    xhttp.open("GET", todayURL, true);
    xhttp.send();
}

function weatherNextDays() {
    if (nextDaysDone) {
        return;
    }

    nextDaysDone = true;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var datos = JSON.parse(this.response).daily;
            var s = "<tbody>" +
                "<tr><th>Día</th><th>Pronóstico</th><th>Temperatura Mínima</th><th>Temperatura Máxima</th><th>Humedad</th></tr>";

            for (var i = 0; i < datos.data.length; ++i) {
                var dato = datos.data[i];

                s += "<tr><td>" + moment(dato.time * 1000).format("DD-MM") + "</td>" +
                    "<td>" + dato.summary + "</td>" +
                    "<td>" + Math.round(dato.temperatureMin) + "ºC</td>" +
                    "<td>" + Math.round(dato.temperatureHigh) + "ºC</td>" +
                    "<td>" + Math.round(dato.humidity * 100) + "%</td>" +
                    "</tr>"
            }
            s += "</tbody>";

            document.getElementById("tableWeek").innerHTML = s;
        }
    };
    xhttp.open("GET", weekURL, true);
    xhttp.send();
}

function navegation() {
    var x = document.getElementById("week");
    if (x.style.display === "none") {
        x.style.display = "block";
        weatherNextDays();
    } else {
        x.style.display = "none";
    }

    x = document.getElementById("now");
    if (x.style.display === "none") {
        x.style.display = "block";
        weatherNow();
        weatherNextHours();
    } else {
        x.style.display = "none";
    }

    x = document.getElementById("linkWeek");
    if (x.className === "") {
        x.className = "active";
    } else {
        x.className = "";
    }

    x = document.getElementById("linkNow");
    if (x.className === "") {
        x.className = "active";
    } else {
        x.className = "";
    }
}

