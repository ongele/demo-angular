import { Component, OnInit } from '@angular/core';
import {Weather} from "../weather";
import {WeatherService} from "../weather.service";

import * as moment from 'moment';

@Component({
  selector: 'weather-now',
  templateUrl: './weather-now.component.html',
  styleUrls: ['./weather-now.component.css']
})
export class WeatherNowComponent implements OnInit {

  weather:Weather;

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeatherNow();
  }

  getWeatherNow():void {
    this.weatherService.getWeatherNow()
      .subscribe(weather => {
        this.weather = weather;
        this.weather.currently.time = moment(this.weather.currently.time*1000).format('HH:mm:ss');
        this.weather.currently.temperature = Math.round(weather.currently.temperature)
      });
  }

  round(number:number):number {
    return Math.round(number);
  }
}
