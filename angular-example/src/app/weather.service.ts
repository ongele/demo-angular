import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Weather} from "./weather";
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import {environment} from "../environments/environment.prod";

@Injectable()
export class WeatherService {

  private weatherUrl = environment.backendURL;

  constructor(
    private http: HttpClient) { }

  getWeatherNow (): Observable<Weather> {
    return this.http.get<Weather>(this.weatherUrl+"/now")
      .pipe(
        //tap(weather => console.log(`fetched weather`, weather)),
        catchError(this.handleError('getWeatherNow', null))
      );
  }

  getWeatherToday (): Observable<Weather> {
    return this.http.get<Weather>(this.weatherUrl+"/nextHours")
      .pipe(
        //tap(weathers => console.log(`fetched weather`, weathers)),
        catchError(this.handleError('getWeatherNow', null))
      );
  }

  getWeatherWeek (): Observable<Weather> {
    return this.http.get<Weather>(this.weatherUrl+"/nextDays")
      .pipe(
        //tap(weathers => console.log(`fetched weather`, weathers)),
        catchError(this.handleError('getWeatherNow', null))
      );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
