import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageWeekComponent }   from './page-week/page-week.component';
import { PageTodayComponent }      from './page-today/page-today.component';

const routes:Routes = [
  {path: '', redirectTo: '/hoy', pathMatch: 'full'},
  {path: 'semana', component: PageWeekComponent},
  {path: 'hoy', component: PageTodayComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
