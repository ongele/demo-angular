import { Component, OnInit } from '@angular/core';
import {WeatherService} from "../weather.service";
import {Weather} from "../weather";

import * as moment from 'moment';

@Component({
  selector: 'weather-week',
  templateUrl: './weather-week.component.html',
  styleUrls: ['./weather-week.component.css']
})
export class WeatherWeekComponent implements OnInit {

  weathers:Weather;

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeatherWeek();
  }

  getWeatherWeek():void {
    this.weatherService.getWeatherWeek()
      .subscribe(weathers => {this.weathers = weathers.daily.data});
  }

  round(number:number):number {
    return Math.round(number);
  }

  format(time: number): string{
    return moment(time*1000).format('DD-MM')
  }
}
