import { Component, OnInit } from '@angular/core';
import {WeatherService} from "../weather.service";
import {Weather} from "../weather";

import * as moment from 'moment';

@Component({
  selector: 'weather-today',
  templateUrl: './weather-today.component.html',
  styleUrls: ['./weather-today.component.css']
})
export class WeatherTodayComponent implements OnInit {

  weathers:Weather;

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeatherToday();
  }

  getWeatherToday():void {
    this.weatherService.getWeatherToday()
      .subscribe(weathers => {this.weathers = weathers.hourly.data});
  }

  round(number:number):number {
    return Math.round(number);
  }

  format(time: number): string{
    return moment(time*1000).format('HH:mm')
  }
 }
