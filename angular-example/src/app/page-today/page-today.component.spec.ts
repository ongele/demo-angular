import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTodayComponent } from './page-today.component';

describe('PageTodayComponent', () => {
  let component: PageTodayComponent;
  let fixture: ComponentFixture<PageTodayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTodayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
