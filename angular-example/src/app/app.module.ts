import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { PageWeekComponent }   from './page-week/page-week.component';
import { PageTodayComponent }      from './page-today/page-today.component';
import { WeatherService } from './weather.service';
import { WeatherNowComponent } from './weather-now/weather-now.component';
import { WeatherTodayComponent } from './weather-today/weather-today.component';
import { WeatherWeekComponent } from './weather-week/weather-week.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    PageWeekComponent,
    PageTodayComponent,
    WeatherNowComponent,
    WeatherTodayComponent,
    WeatherWeekComponent
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
